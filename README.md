# MY01 QA Test

## Getting started

Welcome to the Software QA technical challenge!

This repository contains a simple React Native Expo app. The app is composed of two screens:
1. Login screen.
2. Action screen.

Unfortunately the developer did not take the time to develop tests.

## The challenge!

The code coverage is currently 0%. Your goal is to increase the coverage by implementing different type of tests.

Start by forking this repo to a **private** personal repository. You can use Gitlab, Github, or any other collaborative version control tool.
Then, add a testing framework such as [Jest](https://jestjs.io) and start writing some tests :)

Using a combination of unit, component and integration tests is recommended. A good resource to get started with React
Native app testing is React Native's [official website](https://reactnative.dev/docs/testing-overview).

### Bonus 1
Implement End-to-End tests using a framework such as [Detox](https://github.com/wix/Detox). 
In end-to-end (E2E) tests, you verify the app is working as expected on a device (or a simulator / emulator) from the user perspective.

### Bonus 2
Setup a Gitlab CI/CD pipeline to run your tests with each commit!
Take a look at Gitlab [documentation](https://docs.gitlab.com/ee/ci/).

### Submission
Once satisfied with your work, invite Eric Schaal: [Github](https://github.com/ericschaal) or [Gitlab](https://gitlab.com/ericschaal) as a collaborator (read-only is ok) and send us an email.

### Guidelines

- **Only submit your own work.** 
- You are allowed to add any framework/library to this project. But keep in mind that we might ask you to justify the reason during the technical interview.
- You are also allowed to refactor the codebase **but do not add or remove any feature**!
- Do not `eject` the expo app. [Please don't.](https://docs.expo.dev/expokit/eject/)
- Bonuses are optional. You can do both, just one or none.
- If you discover a bug while testing, report it as a [Gitlab issue](https://docs.gitlab.com/ee/user/project/issues/).
- Feel free to ask us questions, email us.

## How to run the app?

This guide assumes that you have Xcode and/or Android Studio installed and working. 
It also assumes that you have [Node 12 LTS](https://nodejs.org/en/) or greater installed along with [Yarn](https://yarnpkg.com).

### Install the Expo CLI command line utility
```
# If you don't have expo-cli yet, get it
npm i -g expo-cli
```

### Install project dependencies
```
yarn install
```

### Start the development server.
```
expo start
```
When you run `expo start`.
Expo CLI starts Metro Bundler, which is an HTTP server that compiles the JavaScript code of our app using Babel and serves it to the Expo app.
It also pops up Expo Dev Tools, a graphical interface for Expo CLI.

### Install Expo Go app for iOS and Android

The fastest way to get up and running is to use the Expo Go app on your iOS or Android device.
Expo Go allows you to open up apps that are being served through Expo CLI.

- 🤖 [Android Play Store](https://play.google.com/store/apps/details?id=host.exp.exponent) - Android Lollipop (5) and greater. 
- 🍎 [iOS App Store](https://itunes.com/apps/exponent) - iOS 11 and greater.

### Opening the app on your phone/tablet

- 🍎 On your iPhone or iPad, open the default Apple "Camera" app and scan the QR code you see in the terminal or in Expo Dev Tools.
- 🤖 On your Android device, press "Scan QR Code" on the "Projects" tab of the Expo Go app and scan the QR code you see in the terminal or in Expo Dev Tools.

### Using a simulator or emulator?

If you are using a simulator or emulator, you may find the following Expo CLI keyboard shortcuts to be useful:
- Pressing `i` will open in an iOS simulator.
- Pressing `a` will open in an Android emulator or connected device.
- Pressing `w` will open in your browser. Expo supports all major browsers.

### Questions?

Take a look at Expo's [Get Started](https://docs.expo.dev) guide!

## How to use the app?

### Login to the app.

Use the following credentials to login:

| Username | Password | Role           |
|----------|----------|----------------|
| user     | user     | USER           |
| hr       | hr       | HR             |
| ceo      | ceo      | SUPER_COOL_CEO |

### Performing actions!

Once logged in, the app will welcome you with the action screen. On this screen you can press a button to:
 - Make coffee
 - Hire an employee
 - Talk to investors
 
**Depending on your role you may not be allowed to perform some actions!**


## Useful Websites
 - [Expo](https://expo.dev): Expo is a framework and a platform for universal React applications.
 - [React Native](https://reactnative.dev): Create native apps for Android and iOS using React
 - [Redux](https://redux.js.org): A Predictable State Container for JS Apps
 - [Redux Toolkit](https://redux-toolkit.js.org): The official, opinionated, batteries-included toolset for efficient Redux development
 - [Native Base](https://nativebase.io): NativeBase is an accessible, utility-first component library that helps you build consistent UI across Android, iOS and Web.
 - [Jest](https://jestjs.io): Jest is a delightful JavaScript Testing Framework with a focus on simplicity.
 - [Detox](https://wix.github.io/Detox/): Gray box end-to-end testing and automation framework for mobile apps
