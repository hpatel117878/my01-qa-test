export enum Action {
  MAKE_COFFEE,
  HIRE_EMPLOYEE,
  TALK_TO_INVESTORS,
}
