import { useAppSelector } from "./store/hooks";
import { selectIsUserLoggedIn } from "./features/login/loginSlice";
import { ActionsScreen } from "./features/runActions/ActionsScreen";
import { SignInScreen } from "./features/login/SignInScreen";
import React from "react";
import FakeData from "../db/FakeData";
import AuthenticationService from "../auth/AuthenticationService";
import AuthorizationService from "../auth/AuthorizationService";

const db = new FakeData();
db.loadFakeData();

const authenticationService = new AuthenticationService(db);
const authorizationService = new AuthorizationService();

export const Root = () => {
  const isUserLoggedIn = useAppSelector(selectIsUserLoggedIn);
  if (isUserLoggedIn) {
    return (
      <ActionsScreen
        auth={authenticationService}
        authz={authorizationService}
      />
    );
  } else {
    return <SignInScreen auth={authenticationService} />;
  }
};
