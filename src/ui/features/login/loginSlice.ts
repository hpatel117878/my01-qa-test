import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { IUser } from "../../../auth/IUser";
import { RootState } from "../../store/store";

// Define a type for the slice state
interface CounterState {
  currentUser: IUser | null;
}

// Define the initial state using that type
const initialState: CounterState = {
  currentUser: null,
};

export const loginSlice = createSlice({
  name: "login",
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    userLogin: (state, action: PayloadAction<IUser>) => {
      state.currentUser = action.payload;
    },
    userLogout: (state) => {
      state.currentUser = null;
    },
  },
});

export const { userLogin, userLogout } = loginSlice.actions;

// Other code such as selectors can use the imported `RootState` type
export const selectCurrentUser = (state: RootState) => state.login.currentUser;
export const selectIsUserLoggedIn = (state: RootState) =>
  selectCurrentUser(state) !== null;

export const loginActions = loginSlice.actions;
export default loginSlice.reducer;
