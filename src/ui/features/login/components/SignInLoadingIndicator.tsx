import React from "react";
import { HStack, Spinner, Text } from "native-base";
import { IHStackProps } from "native-base/lib/typescript/components/primitives/Stack/HStack";

export const SignInLoadingIndicator = (props: IHStackProps) => {
  return (
    <HStack {...props} space={2} alignItems="center">
      <Spinner color="primary.400" accessibilityLabel="Sign in in progress" />
      <Text color="primary.400" fontWeight="bold" fontSize="md">
        Signing in...
      </Text>
    </HStack>
  );
};
