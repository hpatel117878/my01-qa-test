import React from "react";
import { HStack, Image, Text, VStack } from "native-base";
import logo from "../../../../../assets/images/my01-logo-white.png";

export const SignInHeader = () => (
  <VStack px="4" mt="4" mb="5">
    <HStack space="2">
      <Image height={20} alt="MY01 Logo" resizeMode="contain" source={logo} />
      <VStack space="2">
        <Text fontSize="3xl" fontWeight="bold" color="white">
          Welcome back,
        </Text>
        <Text color="primary.50" fontSize="md" fontWeight="normal">
          Sign in to continue
        </Text>
      </VStack>
    </HStack>
  </VStack>
);
