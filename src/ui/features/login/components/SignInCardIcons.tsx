import { Icon, IconButton } from "native-base";
import { Ionicons } from "@expo/vector-icons";
import React from "react";

export const PasswordIcon = () => (
  <Icon size="5" ml="1" mr="3" color="muted.400" as={Ionicons} name="key" />
);
export const UsernameIcon = () => (
  <Icon size="5" ml="1" mr="3" color="muted.400" as={Ionicons} name="mail" />
);

export const ShowPasswordIcon = ({
  showPass,
  setShowPass,
}: {
  showPass: boolean;
  setShowPass: (value: boolean) => void;
}) => {
  return (
    <IconButton
      variant="unstyled"
      icon={
        <Icon
          size="5"
          color="coolGray.400"
          as={Ionicons}
          name={showPass ? "eye-off" : "eye"}
        />
      }
      onPress={() => setShowPass(!showPass)}
    />
  );
};
