import React from "react";
import { Text, View } from "react-native";

export const SignInCardFooter = () => (
  <View>
    <View>
      <Text>I'm a new user. Request an account.</Text>
    </View>
  </View>
);
