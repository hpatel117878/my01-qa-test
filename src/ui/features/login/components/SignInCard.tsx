import React, { useCallback, useState } from "react";
import { HStack, Link, Text, View, VStack } from "native-base";
import AuthenticationService from "../../../../auth/AuthenticationService";
import { SignInForm } from "./SignInForm";
import { SignInCardAlternateLoginOptions } from "./SignInCardAlternateLoginOptions";
import { Alert } from "react-native";

export interface ISignInCardProps {
  auth: AuthenticationService;
}

export const SignInCard = ({ auth }: ISignInCardProps) => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [rememberMe, setRememberMe] = useState(true);
  const [loading, setLoading] = useState(false);

  const onSignInPressed = useCallback(() => {
    const login = async () => {
      try {
        setLoading(true);
        await auth.login(username, password);
      } catch (e: any) {
        Alert.alert("Failed to login.", e.message, undefined);
      } finally {
        setLoading(false);
        setPassword("");
        setUsername("");
      }
    };
    login().catch(console.error);
  }, [username, rememberMe, password]);

  return (
    <View style={{ flex: 1 }}>
      <VStack flex="1" px="6" py="9" backgroundColor="white" borderRadius="2xl">
        <VStack>
          <VStack space="3">
            <SignInForm
              loading={loading}
              usernameValue={username}
              onUsernameTextChanged={setUsername}
              passwordValue={password}
              onPasswordTextChanged={setPassword}
              rememberMeValue={rememberMe}
              onRememberMePressed={setRememberMe}
              onSubmitPressed={onSignInPressed}
              onForgotPasswordPressed={() => {}}
            />
            <SignInCardAlternateLoginOptions />
          </VStack>
        </VStack>
        <HStack
          mb="4"
          space="1"
          safeAreaBottom
          alignItems="center"
          justifyContent="center"
          mt={{ base: "auto" }}
        >
          <Text color="coolGray.800">Don't have an account?</Text>
          <Link
            _text={{
              fontWeight: "bold",
              textDecoration: "none",
            }}
            color="primary.900"
            onPress={() => {}}
          >
            Sign up
          </Link>
        </HStack>
      </VStack>
    </View>
  );
};
