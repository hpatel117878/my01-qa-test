import { Box, IInputProps, Input } from "native-base";
import { ColorValue, Platform, StyleSheet } from "react-native";
import Animated, {
  interpolate,
  useAnimatedStyle,
  useSharedValue,
  withTiming,
} from "react-native-reanimated";
import React, { useCallback } from "react";

export interface IFloatingLabelInputProps extends IInputProps {
  containerWidth?: number;
  labelBackgroundColor?: ColorValue;
  labelColor?: ColorValue;
  label: string;
}

export const FloatingLabelInput = ({
  labelColor = "#9ca3af",
  labelBackgroundColor = "#fff",
  label,
  containerWidth,
  value,
  isDisabled,
  ...rest
}: IFloatingLabelInputProps) => {
  const animationValue = useSharedValue<number>(0); // 0 to 1

  const onBlur = useCallback(() => {
    if (!value) {
      animationValue.value = withTiming(0, { duration: 200 });
    }
  }, [animationValue, value]);

  const onFocus = useCallback(() => {
    animationValue.value = withTiming(1, { duration: 200 });
  }, [animationValue]);

  const labelContainerAnimatedStyle = useAnimatedStyle(
    () => ({
      backgroundColor: isDisabled ? "coolGray.100" : labelBackgroundColor,
      top: interpolate(animationValue.value, [0, 1], [18, -7.5]),
    }),
    [labelBackgroundColor, isDisabled]
  );

  const labelAnimatedStyle = useAnimatedStyle(() => {
    if (Platform.OS === "android") {
      return {
        fontSize: interpolate(animationValue.value, [0, 1], [14, 12]),
        color: labelColor,
      };
    } else {
      return {
        fontSize: interpolate(animationValue.value, [0, 1], [14, 12]),
        marginTop: interpolate(animationValue.value, [0, 1], [-3, 0]),
        color: labelColor,
      };
    }
  }, [labelColor]);

  return (
    <Box w={containerWidth}>
      <Animated.View
        pointerEvents="none"
        style={[styles.labelContainer, labelContainerAnimatedStyle]}
      >
        <Animated.Text style={[styles.labelStyle, labelAnimatedStyle]}>
          {label}
        </Animated.Text>
      </Animated.View>
      <Input
        size="xl"
        isDisabled={isDisabled}
        style={styles.inputStyle}
        borderColor="coolGray.300"
        borderRadius={4}
        value={value}
        {...rest}
        _disabled={{ backgroundColor: "coolGray.100" }}
        paddingTop={3}
        paddingBottom={3}
        onFocus={onFocus}
        onBlur={onBlur}
      />
    </Box>
  );
};

const styles = StyleSheet.create({
  labelContainer: {
    position: "absolute",
    left: 32,
    zIndex: 5,
    paddingLeft: 3,
    paddingRight: 3,
  },
  inputStyle: {},
  labelStyle: {
    fontWeight: "500",
  },
});
